FROM debian:bookworm-slim

MAINTAINER http://dedukto.com version: 0.1

RUN set -ex; \
	apt-get update; \
    apt-get install -y --no-install-recommends \
    coreutils \
    python3 python3-pip python3-venv \
    gnupg ca-certificates wget git \
    sqlite3 \
    nodejs npm \
	; \
	rm -rf /var/lib/apt/lists/*

# make the "en_US.UTF-8" locale so postgres will be utf-8 enabled by default.
RUN set -eux; \
	if [ -f /etc/dpkg/dpkg.cfg.d/docker ]; then \
    # if this file exists, we're likely in "debian:xxx-slim", and locales are
    # thus being excluded so we need to remove that exclusion (since we need locales).
		grep -q '/usr/share/locale' /etc/dpkg/dpkg.cfg.d/docker; \
		sed -ri '/\/usr\/share\/locale/d' /etc/dpkg/dpkg.cfg.d/docker; \
		! grep -q '/usr/share/locale' /etc/dpkg/dpkg.cfg.d/docker; \
	fi; \
	apt-get update; apt-get install -y --no-install-recommends locales; rm -rf /var/lib/apt/lists/*; \
	localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8
ENV LANG en_US.utf8

RUN set -eux; \
	apt-get update; \
	apt-get install -y --no-install-recommends \
		libnss-wrapper \
		xz-utils \
		zstd \
	; \
	rm -rf /var/lib/apt/lists/*

run set -eux; \
    npm install --global yarn

RUN set -eux; \
    cd /opt; \
    git clone https://github.com/mnzpk/WikiNav.git; \
    cd WikiNav; \
    git checkout ad4d9a78c9e94a9a8b8697b4c01c8bf6e8d969c9

COPY docker-WikiNav-standalone.patch /opt/WikiNav/

RUN cd /opt/WikiNav; \
    git apply /opt/WikiNav/docker-WikiNav-standalone.patch

RUN cd /opt/WikiNav/server/api; \
    pip install --force-reinstall --break-system-packages --upgrade -r requirements.txt

RUN cd /opt/WikiNav/client; \
    npm install --loglevel verbose; \
    npx --node-options="--inspect" browserslist@latest --update-db

ENV DATA_TSV /var/lib/WikiNav/data_clickstream
ENV DATA_DB  /var/lib/WikiNav/data_db
ENV LOG /var/lib/WikiNav/log

RUN mkdir -pv /opt/bin; \
    echo "\
#!/bin/bash -x \n\
cd /opt/WikiNav/server/api \n\
/usr/bin/nohup flask --app /opt/WikiNav/server/api/app/api.py run --host=\"0.0.0.0\" --debug &>> \"${LOG}/wikinav-api.log\" & \n\
cd /opt/WikiNav/client \n\
/usr/bin/nohup bash -c \"export NODE_OPTIONS=--openssl-legacy-provider && npm start\" &>> \"${LOG}/wikinav-client.log\" & \n\
echo \"API server and client was started:\" \n\
jobs -l \n\
for PID in \$(jobs -p); do \n\
    wait $PID \n\
done \n\
    " > /opt/bin/wikinav.sh; \
    cat /opt/bin/wikinav.sh; \
    chmod -v a+x /opt/bin/wikinav.sh; \
    mkdir -pv "$DATA_TSV" && chmod 777 "$DATA_TSV" && ln -sv "$DATA_TSV" /opt/WikiNav/server/api/tsv; \
    mkdir -pv "$DATA_DB" && chmod 777 "$DATA_DB" && ln -sv "$DATA_DB" /opt/WikiNav/server/api/db; \
    mkdir -pv "$LOG" && chmod 777 "$LOG"

# WikiNav data volume.
VOLUME /var/lib/WikiNav

ENTRYPOINT ["/bin/bash"]

# `^C` signal equivalent to `kill $(jobs -p)`.
STOPSIGNAL SIGINT

# WkiNav client: 3000/tcp .
EXPOSE 3000/tcp
# WikiNav api: 5000/tcp.
EXPOSE 5000/tcp

# For override default start:
# ```
# $ docker run wikinav-standalone bash
# $ docker exec -it wikinav-standalone bash
# ```
CMD ["-x", "-c", "/opt/bin/wikinav.sh"]
