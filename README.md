# WikiNav-standalone

This project uses WikiNav project (<https://github.com/mnzpk/WikiNav>) and patch it to be run locally from a docker contain.

## Creating the container

For deploy the container:

```
$ # Downlaod `docker-compose.yml` file.
$ wget https://gitlab.com/dedukto-etl/wikinav-standalone/-/raw/main/docker-compose.yml?inline=false
$ # Deploy the container.
$ docker compose up wikinav-standalone
```

After this container begin, a `data` directory is created. Inside it:

- `data/data_clickstream`: `tsv` files downloaded from <https://dumps.wikimedia.org/other/clickstream/>. It is organized by periods (`YYYY-MM` where `YYYY` is year and `MM` is month). It is used as cache if there are previous or external downloaded files.
- `data/data_db`: SQLite databases derived from `tsv` files used by API. It is organized by periods (`YYYY-MM` where `YYYY` is year and `MM` is month).
- `data/log/wikinav-api.log`: Logs of server API.
- `data/log/wikinav-client.log`: Logs of server client.

Example of this structure:

```
$ find data
data
data/data_clickstream
data/data_clickstream/2023-02
data/data_clickstream/2023-02/clickstream-eswiki-2023-02.tsv.gz
data/data_clickstream/2023-02/clickstream-enwiki-2023-02.tsv.gz
data/data_clickstream/2023-01
data/data_clickstream/2023-01/clickstream-eswiki-2023-01.tsv.gz
data/data_clickstream/2023-01/clickstream-enwiki-2023-01.tsv.gz
data/data_db
data/data_db/2023-01
data/data_db/2023-01/clickstream-eswiki-2023-01.db
data/data_db/2023-01/clickstream-enwiki-2023-01.db
data/data_db/2023-02
data/data_db/2023-02/clickstream-eswiki-2023-02.db
data/data_db/2023-02/clickstream-enwiki-2023-02.db
data/log
data/log/wikinav-api.log
data/log/wikinav-client.log
```

## Downloading new periods or languages.

For download new period or language (or if it just downloaded) and create SQLite database:

```
$ L="es" && P="2023-01" && docker exec -it wikinav-standalone bash -c "cd /opt/WikiNav/server/api && python3 scripts/dump_to_sqlite.py -w /var/lib/WikiNav/data_clickstream/ -d $L $P /var/lib/WikiNav/data_db/${P}/"
```

where `L` is the language and `P` is the period (`YYYY-MM` where `YYYY` is year and `MM` is month).

After new period/language (one or more) are processed, it is necessary restart container:

```
$ docker stop wikinav-standalone
$ docker start wikinav-standalone
```

## Accessing to the services.

After docker container began (inspect the logs because server client is slow to start), services are accessible in:

- <http://127.0.0.1:3000/?language=es&title=Programaci%C3%B3n_l%C3%B3gica>: WikiNav UI for queries (in this case for `es` language and `Programación lógica` term.
- <http://127.0.0.1:5000/api/v1/latest/meta>: API listing languages and last period.
- <http://127.0.0.1:5000/api/v1/es/IBM/destinations/latest>: API query for `es` language and `IBM` term.

## Creating image

Docker credentials are stored in `$HOME/.docker/config.json` (<https://docs.docker.com/engine/reference/commandline/login/>). It is managed by (in this project I pull the image to Gitlab):

```
$ docker login registry.gitlab.com -u $USER -p $TOKEN
```

Creating the image (must login first):

```
$ docker-build.sh
```

## Pulling the image

Executing:

```
$ docker pull "registry.gitlab.com/dedukto-etl/wikinav-standalone:v0.1"
```
