#!/bin/bash -x

TAG="registry.gitlab.com/dedukto-etl/wikinav-standalone:v0.1"

# docker login registry.gitlab.com -u USER -p TOKEN
docker build . -t "${TAG}"
docker push "${TAG}"
